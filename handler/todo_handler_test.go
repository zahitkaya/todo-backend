package handler

import (
	"awesomeProject2/controller"
	"awesomeProject2/model"
	error2 "awesomeProject2/model/common/error"
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_ShouldReturnEmptyTodoList(t *testing.T) {
	newController := gomock.NewController(t)
	defer newController.Finish()

	todos := make([]model.Todo, 0)
	service := controller.NewMockService(newController)
	service.EXPECT().FindAll().Return(todos).Times(1)

	handler := TodoHandler{service}
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/todos", nil)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	expected := `[]
`
	if assert.NoError(t, handler.HandleGetAll(ctx)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, expected, rec.Body.String())
	}
}

func Test_ShouldFindAllTodos(t *testing.T) {
	newController := gomock.NewController(t)
	defer newController.Finish()

	var todos []model.Todo
	todos = append(todos, model.Todo{
		Title:     "test todo",
		Completed: false,
	})

	service := controller.NewMockService(newController)
	service.EXPECT().FindAll().Return(todos).Times(1)
	handler := TodoHandler{service}

	expected := `[{"title":"test todo","completed":false}]
`

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/todos", nil)
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	if assert.NoError(t, handler.HandleGetAll(ctx)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, expected, rec.Body.String())
	}
}

func Test_ShouldCreateTodo(t *testing.T) {
	newController := gomock.NewController(t)
	defer newController.Finish()

	request := model.CreateTodoRequest{Title: "test todo"}
	requestAsString := `{"title":"test todo"}`
	expected := model.Todo{
		//ID:        "12345",
		Title:     "test todo",
		Completed: false,
	}
	expectedAsString := `{"title":"test todo","completed":false}
`

	service := controller.NewMockService(newController)
	service.EXPECT().Create(gomock.Eq(request)).Return(expected).Times(1)

	handler := TodoHandler{service}
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/todos", strings.NewReader(requestAsString))
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	if assert.NoError(t, handler.HandleCreate(ctx)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
		assert.Equal(t, expectedAsString, rec.Body.String())
	}
}

func Test_ShouldReturnBadRequestErrorWhenCreatingTodo(t *testing.T) {
	newController := gomock.NewController(t)
	defer newController.Finish()

	request := model.CreateTodoRequest{}
	requestAsString := `{}`
	expected := error2.BadRequestError{Message: "test bad request error message"}
	expectedAsString := `{"message":"test bad request error message"}
`
	service := controller.NewMockService(newController)
	service.EXPECT().Create(gomock.Eq(request)).Return(expected).Times(1)

	handler := TodoHandler{service}
	e := echo.New()
	req := httptest.NewRequest(http.MethodPost, "/todos", strings.NewReader(requestAsString))
	req.Header.Set(echo.HeaderAccept, echo.MIMEApplicationJSON)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	ctx := e.NewContext(req, rec)

	if assert.NoError(t, handler.HandleCreate(ctx)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
		assert.Equal(t, expectedAsString, rec.Body.String())
	}
}

