package handler

import (
	"awesomeProject2/model"
	error2 "awesomeProject2/model/common/error"
	"awesomeProject2/controller"
	"github.com/labstack/echo"
)

type TodoHandler struct {
	service controller.Service
}

type Handler interface {
	HandleGetAll(ctx echo.Context) error
	HandleCreate(ctx echo.Context) error
	//HandleUpdate(ctx echo.Context) error
	//HandleDelete(ctx echo.Context) error
}

func NewTodoHandler(service controller.Service) Handler {
	return TodoHandler{service}
}

func (handler TodoHandler) HandleGetAll(ctx echo.Context) error {
	return ctx.JSON(200, handler.service.FindAll())
}

func (handler TodoHandler) HandleCreate(ctx echo.Context) error {
	var request model.CreateTodoRequest
	_ = ctx.Bind(&request)
	response := handler.service.Create(request)
	error := extractError(response)
	if error != nil {
		return ctx.JSON(error.GetStatus(), error)
	}
	return ctx.JSON(201, response)
}
/*
func (handler TodoHandler) HandleUpdate(ctx echo.Context) error {
	var request model.UpdateTodoRequest
	_ = ctx.Bind(&request)
	id := ctx.Param("id")
	response := handler.controller.Update(id, request)
	error := extractError(response)
	if error != nil {
		return ctx.JSON(error.GetStatus(), error)
	}
	return ctx.JSON(202, response)
}

func (handler TodoHandler) HandleDelete(ctx echo.Context) error {
	id := ctx.Param("id")
	handler.controller.Delete(id)
	return ctx.NoContent(204)
}

 */

func extractError(i interface{}) error2.Error {
	error, ok := i.(error2.Error)
	if ok {
		return error
	}
	return nil
}
