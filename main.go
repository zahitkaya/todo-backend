package main

import (
	"awesomeProject2/configuration"
	"awesomeProject2/controller"
	"awesomeProject2/handler"
	"awesomeProject2/repository"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"os"
)

func main() {
	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	config := new(configuration.MongoConfiguration).Init(getDBUri(), getDBName())

	todoRepository := repository.NewTodoRepository(config.Database().Collection("todos"))
	todoService := controller.NewTodoService(todoRepository)
	todoHandler := handler.NewTodoHandler(todoService)
	e.GET("/todos", todoHandler.HandleGetAll)
	e.POST("/todos", todoHandler.HandleCreate)


	e.Logger.Fatal(e.Start(":8080"))
}

func getDBUri() string {
	uri := os.Getenv("MONGODB_URI")
	if uri == "" {
		return "mongodb://localhost:27017"
	}
	return uri
}

func getDBName() string {
	return "todo-controller"
}

