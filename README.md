# Sample Todo App Backend (Golang)

## Compiling Project

Initiliaze Golang

$ go install


Compiling 

$ go build -o main .


### UNIT Testing

Run your unit tests

$ go test awesomeProject2/controller
$ go test awesomeProject2/handler


## Run Project in Docker

$ docker-compose up -d

## Requests
### Get Task List
  <a>
    <img src="images/getRequest.png" alt="Logo" >
  </a>
  
  ### Create a new Task
  
  <a>
    <img src="images/postRequest.png" alt="Logo">
   </a>
