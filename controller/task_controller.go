package controller

import (
	"awesomeProject2/model"
	err "awesomeProject2/model/common/error"
	"awesomeProject2/repository"
)

type Service interface {
	FindAll() interface{}
	Create(i interface{}) interface{}
}

type TodoService struct {
	repository repository.Repository
}

func NewTodoService(repository repository.Repository) Service {
	return TodoService{repository}
}

func (s TodoService) FindAll() interface{} {
	return s.repository.FindAll()
}

func (s TodoService) Create(i interface{}) interface{} {
	request, ok := i.(model.CreateTodoRequest)
	if !ok {
		return err.BadRequestError{Message: "Invalid request!"}
	}

	if request.Title == "" {
		return err.BadRequestError{Message: "Title cannot be empty!"}
	}

	todo := model.Todo{
		Title:     request.Title,
		Completed: false,
	}
	return s.repository.Create(todo)
}
