package controller

import (
	"awesomeProject2/model"
	err "awesomeProject2/model/common/error"
	"awesomeProject2/repository"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_ShouldFindAllTodos(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	var todoList []model.Todo
	todoList = append(todoList, model.Todo{
		Title:     "test todo",
		Completed: false,
	})

	repository := repository.NewMockRepository(controller)
	repository.EXPECT().FindAll().Return(todoList).Times(1)
	service := TodoService{repository}

	all := service.FindAll().([]model.Todo)
	assert.NotNil(t, all)
	assert.NotEmpty(t, all)
	assert.Equal(t, 1, len(all))
	assert.Equal(t, "test todo", all[0].Title)
	assert.False(t, all[0].Completed)
}

func Test_ShouldCreateTodo(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	repository := repository.NewMockRepository(controller)
	repository.EXPECT().Create(model.Todo{Title: "test todo", Completed: false}).Return(model.Todo{
		Title:     "test todo",
		Completed: false,
	}).Times(1)

	service := NewTodoService(repository)
	created := service.Create(model.CreateTodoRequest{Title: "test todo"})

	assert.NotNil(t, created)
	todo := created.(model.Todo)
	assert.Equal(t, "test todo", todo.Title)
	assert.False(t, todo.Completed)
}

func Test_ShouldReturnBadRequestErrorWhenRequestIsEmpty(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := TodoService{nil}
	response := service.Create(nil)

	error, ok := response.(err.BadRequestError)
	if !ok {
		assert.Fail(t, "Expected BadRequestError response")
	}

	assert.NotNil(t, error)
	assert.Equal(t, 400, error.GetStatus())
	assert.Equal(t, "Invalid request!", error.GetMessage())
}

func Test_ShouldReturnBadRequestErrorWhenTitleIsEmpty(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()

	service := TodoService{nil}
	response := service.Create(model.CreateTodoRequest{})

	error, ok := response.(err.BadRequestError)
	if !ok {
		assert.Fail(t, "Expected BadRequestError response")
	}

	assert.NotNil(t, error)
	assert.Equal(t, 400, error.GetStatus())
	assert.Equal(t, "Title cannot be empty!", error.GetMessage())
}
