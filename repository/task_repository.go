package repository

import (
	"awesomeProject2/model"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Repository interface {
	FindAll() interface{}
	Create(i interface{}) interface{}
}

type TodoRepository struct {
	collection *mongo.Collection
}

func NewTodoRepository(collection *mongo.Collection) Repository {
	return TodoRepository{collection: collection}
}

func (r TodoRepository) FindAll() interface{} {
	var todos = make([]model.Todo, 0)
	found, _ := r.collection.Find(context.Background(), bson.D{})
	_ = found.All(context.Background(), &todos)
	return todos
}


func (r TodoRepository) Create(i interface{}) interface{} {
	id, _ := r.collection.InsertOne(context.Background(), i)
	objectId, _ := id.InsertedID.(primitive.ObjectID)
	filter := bson.M{"_id": objectId}
	one := r.collection.FindOne(context.Background(), filter)
	var todo model.Todo
	_ = one.Decode(&todo)
	return todo
}

